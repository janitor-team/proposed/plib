Source: plib
Section: devel
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Standards-Version: 4.0.0
Build-Depends: autoconf,
               automake,
               debhelper (>= 10),
               freeglut3-dev,
               libgl1-mesa-dev | libgl-dev,
               libtool,
               libx11-dev,
               libxi-dev,
               libxmu-dev,
               quilt
Homepage: http://plib.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/plib.git
Vcs-Browser: https://salsa.debian.org/debian/plib

Package: libplib1
Section: libs
Architecture: any
Depends: freeglut3,
         libgl1-mesa-glx | libgl1,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Portability Libraries: Run-time package
 Provides a Joystick interface, a simple GUI built on top of OpenGL,
 some standard geometry functions, a sound library and a simple scene
 graph API built on top of OpenGL.
 .
 This package provides the shared libraries. It should be installed
 automatically by packages which need it.

Package: libplib-dev
Section: libdevel
Architecture: any
Depends: freeglut3-dev,
         libgl1-mesa-dev | libgl-dev,
         libplib1 (= ${binary:Version}),
         ${misc:Depends}
Conflicts: plib1.8.4-dev
Replaces: plib1.8.4-dev
Provides: plib1.8.4-dev
Description: Portability Libraries: Development package
 Provides a Joystick interface, a simple GUI built on top of OpenGL,
 some standard geometry functions, a sound library and a simple scene
 graph API built on top of OpenGL.
 .
 This package provides the static libraries, the header files and the
 development links to the shared libraries. You need this package if you
 want to compile software which needs plib.
